#!/usr/bin/env python
# -*- coding: utf-8 -*-

def equivalence_voisinage(n, v1, v2, bijection):
    if len(v1) != len(v2):
        return False

    for j in v1:
        #test limité aux n premiers sommets de G1
        if (j<n) and not(bijection[j] in v2):
            return False
    return True

# La bijection est un tableau t tel que t[i] = p(i) où p est une bijection.
# G1, G2 sont des graphes définis par liste d'adjacence.
# n : nombre de somets
def tester_isomorphisme(n, G1, G2, bijection):
    for i in range(n):
        voisins_p_i = G2[bijection[i]] # voisins de p(i) dans G2.
        voisins_i   = G1[i]
        if not(equivalence_voisinage(n, voisins_i, voisins_p_i, bijection)):
            return False
    return True

compteur_global = 0
def id():
    global compteur_global
    compteur_global += 1
    return compteur_global-1

# Une partition est représentée par deux dictionnaires:
#  part associe à une classe la liste des sommets appartenant à cette classe.
#  classe associe à un sommet la classe de ce sommet.
class Partition:
    part = []
    classe = []
    def __init__(self, taille):
        self.part = [[] for _ in range(taille)]
        self.classe = [-1 for _ in range(taille)]

def partition_degre(n, G):
    p = Partition(n)
    global compteur_global
    compteur_global = n

    for i in range(n):
        c = len(G[i])
        p.classe[i] = c
        p.part[c].append(i)
    return p



def compter_classes(n_classes, partition, voisinage):
    comptage = [0 for _ in range(n_classes)]
    for x in voisinage:
        comptage[partition.classe[x]] += 1
    return comptage

def liste_equivalents(n_classes, comptage, classe, blacklist, partition, graphe):
    for y in classe:
        if not(y in blacklist):
            comptage_y = compter_classes(n_classes, partition, graphe[y])
            if comptage == comptage_y:
                yield y


def iteration_partitionnement(p1, p2, G1, G2):
    global compteur_global
    n_classes = compteur_global
    compteur_global = 0 #reset
    taille = len(G1)
    n_p1 = Partition(taille)
    n_p2 = Partition(taille)

    changement = 0

    for c1 in range(taille):
        if len(p1.part[c1]) > 0:
            classe_1 = p1.part[c1]
            classe_2 = p2.part[c1]

            traites_1 = []
            traites_2 = []

            first = True

            for x in classe_1:
                if not(x in traites_1):
                    n = id()
                    traites_1.append(x)
                    comptage_x = compter_classes(n_classes, p1, G1[x])

                    for y in liste_equivalents(n_classes, comptage_x, classe_1, traites_1, p1, G1):
                        n_p1.part[n].append(y)
                        n_p1.classe[y] = n
                        traites_1.append(y)

                    for y in liste_equivalents(n_classes, comptage_x, classe_2, traites_2, p2, G2):
                        n_p2.part[n].append(y)
                        n_p2.classe[y] = n
                        traites_2.append(y)

                    n_p1.part[n].append(x)
                    n_p1.classe[x] = n

                    if len(n_p1.part[n]) != len(n_p2.part[n]):
                        print("incohérence de partitionnement.")
                        return n_p1,n_p2,-1

                    if first == True and len(traites_1) != len(classe_1):
                        changement = 1
                    first = False

    print("ITERATION DU PARTITIONNEMENT: "+str(n_classes)+"=>"+str(compteur_global))

    return n_p1,n_p2,changement



def partitionnement_stable(p1, p2, G1, G2):
    chg = 1
    while chg:
        p1,p2,chg = iteration_partitionnement(p1, p2, G1, G2)
        if chg == -1:
            return p1, p2, False
    return p1, p2, True

def choisir_image(n, G1, G2, position, choisi, bijection, raffinement):
    degre_pos = len(G1[position])
    for test in range(bijection[position]+1,n):
        if (test in raffinement) and not(choisi[test]):
            voisins_position = G1[position]
            voisins_test     = G2[test]
            bijection[position] = test
            if equivalence_voisinage(position+1, voisins_position,
                                     voisins_test, bijection):
                return test
    return -1

def backtracking_glouton(n, G1, G2, raffinement=0):
    bijection = [-1]*n
    choisi    = [False]*n
    position  = 0

    degres_1  = partition_degre(n, G1)
    degres_2  = partition_degre(n, G2)

    if raffinement == 2:
        p1, p2, res = partitionnement_stable(degres_1, degres_2, G1, G2)
    elif raffinement == 1:
        p1, p2, res = degres_1, degres_2, True


    if res == False:
        return False

    while(position < n):
        # on doit déterminer l'image de position.
        restriction = [i for i in range(n)] if raffinement == 0 else p2.part[p1.classe[position]]

        #print("position:"+str(position)+" -> "+str(restriction))
        choix = choisir_image(n, G1, G2, position, choisi, bijection, restriction)

        if choix != -1:
            # on avance.
            choisi[choix] = True
            bijection[position] = choix
            position += 1
        else:
            # on recule.
            bijection[position] = -1
            position -= 1
            choisi[bijection[position]] = False
            if position == -1 or (position == 0 and bijection[position] == n-1):
                return False
    return bijection
