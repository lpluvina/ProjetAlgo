CC=gcc
CFLAGS=-I. -lm

DEPS = util.h
OBJ = util.o main.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

main: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)
