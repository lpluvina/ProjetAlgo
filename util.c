#include "util.h"
#include <stdio.h>
#include <math.h>

ull mem_bound(int taille_liste) {return taille_liste*3+1;}
int mem(int val, Liste* liste) {
  for(;liste != 0; liste = liste->next) {
    if(liste->val == val) {
      return 1;
    }
  }
  return 0;
}

ull taille_bound(int taille_liste) {return 2+taille_liste*3;}
int taille(Liste* liste) {
  int c=0;
  for(;liste != 0; liste = liste->next) {
    c++;
  }
  return c;
}

ull append_bound() {return 4;}
Liste* append(int val, Liste* lst) {
  Liste* elem = malloc(sizeof(Liste));
  elem->val = val;
  elem->next = lst;
  return elem;
}

Liste* merge(Liste* l1, Liste* l2) {
  if(l1 == 0 && l2 == 0) {return 0;}
  else if(l1 == 0 && l2 != 0) {return l2;}
  else if(l2 == 0 && l1 != 0) {return l1;}
  else {
    if(l1->val <= l2->val) {
      l1->next = merge(l1->next,l2);
      return l1;
    } else {
      l2->next = merge(l1,l2->next);
      return l2;
    }
  }
}

ull sort_bound(int taille_liste) {
  return 5*taille_liste*log(2+(double)taille_liste);
}

Liste* sort(Liste* l) {
  if(l == 0) {
    return 0;
  } else if(l->next == 0) {
    return l;
  } else {
    // Séparation
    Liste* l1 = 0;
    Liste* l2 = 0;
    while(l != 0) {
      if(l->next == 0) {
        Liste* suivant = l->next;
        l->next = l1;
        l1 = l;
        l = suivant;
      } else {
        Liste* suivant = l->next->next;
        l->next->next = l2;
        l2 = l->next;
        l->next = l1;
        l1 = l;
        l = suivant;
      }
    }
    // Tri
    l1 = sort(l1);
    l2 = sort(l2);
    // Fusion
    return merge(l1,l2);
  }
}

void free_liste(Liste* l) {
  if(l != 0) {
    free_liste(l->next);
    l->next = 0;
    free(l);
  }
}

void free_partition(int n, Partition* p) {
  for(int i=0;i<n;i++) {
    free_liste(p->part[i]);
  }
  free(p->classe);
  free(p->part);
  free(p);
}

void free_graphe(int n, Graphe g) {
  for(int i=0;i<n;i++) {
    free_liste(g[i]);
  }
  free(g);
}




void afficher(int n, int* bijection) {
  for(int i=0;i<n;i++) {
    printf("%i => %i\n",i,bijection[i]);
  }
}

void afficher_liste(Liste* l) {
  printf("[");
  for(;l != 0; l = l->next) {
    printf("%i; ",l->val);
  }
  printf("]\n");
}

void afficher_g(int n, Graphe g) {
  for(int i=0;i<n;i++) {
    printf("%i: ",i);
    afficher_liste(g[i]);
  }
}

int* permutation_aleatoire(int taille) {
  int* perm = malloc(taille*sizeof(int));
  for(int i=0;i<taille;i++) {
    perm[i] = i;
  }
  for(int i=taille-1;i>0;i--) {
    int j = rand() % (i+1);
    int tmp = perm[j];
    perm[j] = perm[i];
    perm[i] = tmp;
  }
  return perm;
}

Graphe generation_erdos_renyi(int n, float p) {
  Graphe G = calloc(n,sizeof(Liste*));
  for(int i=0;i<n;i++) {
    for(int j=0;j<i;j++) {
      if (rand()/(float)RAND_MAX <= p) {
        G[i] = append(j,G[i]);
        G[j] = append(i,G[j]);
      }
    }
  }
  return G;
}

PaireGraphes generer_paire_graphes(int n, float p, int force_isomorphism) {
  Graphe G1 = generation_erdos_renyi(n, p);
  if (force_isomorphism) {
    int* perm = permutation_aleatoire(n);
    Graphe G2 = calloc(n,sizeof(Liste*));
    for(int i=0;i<n;i++) {
      Liste* j = G1[i];
      for(;j != 0;j = j->next) {
        G2[perm[i]] = append(perm[j->val], G2[perm[i]]);
      }
    }
    free(perm);
    PaireGraphes x = {G1,G2};
    return x;
  } else {
    Graphe G2 = generation_erdos_renyi(n, p);
    PaireGraphes x = {G1,G2};
    return x;
  }
}
