#include "stdio.h"
#include "util.h"
#include "stdlib.h"

#define RAFFINEMENT_GLOUTON 0
#define RAFFINEMENT_DEGRES  1
#define RAFFINEMENT_PRECIS  2




int min(int a, int b) {return a<b? a : b;}

ull equivalence_voisinage_bound(int n, int taille_v1, int taille_v2) {
  return 3*taille_v1 + min(taille_v1,n)*(2+mem_bound(taille_v2))+1; // O(taille_v1*taille_v2)
}
int equivalence_voisinage(int n, Liste* v1, Liste* v2, int* bijection) {
  for(;v1 != 0; v1 = v1->next) {
    if(v1->val < n && !mem(bijection[v1->val], v2)) {
      return 0;
    }
  }
  return 1;
}

ull tester_isomorphisme_bound(int n, int m_G1, int m_G2, int degre_max_G) {
  return n*equivalence_voisinage_bound(n,degre_max_G,degre_max_G); // O(nombre d'arêtes)
}
int tester_isomorphisme(int n, Graphe G1, Graphe G2, int* bijection) {
  for(int i=0;i<n;i++) {
    if( !equivalence_voisinage(n, G1[i], G2[bijection[i]], bijection)) {
      return 0;
    }
  }
  return 1;
}

ull nouvelle_partition_bound(int taille) {return 2*taille + 2;}
Partition* nouvelle_partition(int taille) {
  Partition* p = malloc(sizeof(Partition));
  p->part = calloc(taille,sizeof(Liste*));
  p->classe = calloc(taille,sizeof(int));
  return p;
}

ull partition_degre_bound(int n, int degre_max_G) {
  return 1+nouvelle_partition_bound(n)+n*(taille_bound(degre_max_G)+10); // O(nombre d'arêtes)
}
Partition* partition_degre(int n, Graphe G) {
  Partition* p = nouvelle_partition(n);
  for(int i=n-1;i>=0;i--) {
    int c = taille(G[i]);
    p->classe[i] = c;
    Liste* insert = malloc(sizeof(Liste));
    insert->val = i;
    insert->next = p->part[c];
    p->part[c] = insert;
  }
  return p;
}

ull egalite_comptage_bound(int taille_liste_x, int taille_liste_y) {
  return 2*min(taille_liste_x, taille_liste_y);
}
int egalite_comptage(Liste* x, Liste* y) {
  if(x == 0 && y == 0) {
    return 1;
  } else if(x == 0 || y == 0) {
    return 0;
  } else if(x->val == y->val) {
    return egalite_comptage(x->next, y->next);
  } else {
    return 0;
  }
}

ull liste_equivalents_bound(int n_classes, int taille_classe, int degre_max_G) {
  return 2+taille_classe*(3+append_bound()+egalite_comptage_bound(degre_max_G, degre_max_G));
}
Liste* liste_equivalents(int n_classes, Liste** tableau_comptage, Liste* comptage_x, Liste* classe, char* blacklist) {
  Liste* resultat = 0;
  for(;classe != 0; classe = classe->next) {
    if(!blacklist[classe->val]) {
      if (egalite_comptage(comptage_x, tableau_comptage[classe->val])) {
        resultat = append(classe->val, resultat);
      }
    }
  }
  return resultat;
}

ull initialiser_tableau_comptage_bound(int n,int degre_max_G,int m) {
  return n + 2*m*(2+append_bound()) + n*sort_bound(degre_max_G); // O(n + m*log(n))
}
Liste** initialiser_tableau_comptage(int n, Partition *p1, Graphe G1)  {
  Liste** tbl = calloc(n,sizeof(Liste*));
  for(int x=0;x<n;x++) {
    for(Liste* voisins_it = G1[x]; voisins_it != 0; voisins_it = voisins_it->next) {
      tbl[x] = append(p1->classe[voisins_it->val],tbl[x]);
    }
    tbl[x] = sort(tbl[x]);
  }
  return tbl;
}

ull iteration_partitionnement_bound(int n, int degre_max_G, int m) {
  int total = 0;
  total += 2*nouvelle_partition_bound(n);
  total += 2;
  total += 2*initialiser_tableau_comptage_bound(n, degre_max_G, m);
  total += 2*n; // Calloc
  total += n*(20+n*liste_equivalents_bound(1, n, degre_max_G)); // Boucle principale dans le pire des cas (Séparation de quelques classes en n classes)
  total += 10+2*n; // Free
  return total; // O(n²*m) dans le pire des cas
}
PairePartitionsRetour iteration_partitionnement(int n, Partition *p1, Partition *p2, Graphe G1, Graphe G2) {
  Partition* n_p1 = nouvelle_partition(n);
  Partition* n_p2 = nouvelle_partition(n);
  int changement = 0;
  int id = 0;

  Liste** tableau_comptage_1 = initialiser_tableau_comptage(n, p1, G1);
  Liste** tableau_comptage_2 = initialiser_tableau_comptage(n, p2, G2);

  char* traites_1 = calloc(n,sizeof(char));
  char* traites_2 = calloc(n,sizeof(char));

  for(int c1 = 0; c1 < n; c1++) {
    if(p1->part[c1] != 0) {
      Liste* classe_1 = p1->part[c1];
      Liste* classe_2 = p2->part[c1];


      int first = 1;


      for(Liste* it_x = classe_1; it_x != 0; it_x = it_x->next) {
        int x = it_x->val;
        if(!traites_1[x]) {
          int n_classe = id;
          id++;

          int ntraites = 0;

          Liste* ylist_1 = liste_equivalents(n, tableau_comptage_1, tableau_comptage_1[x], classe_1, traites_1);
          for(Liste* y_it = ylist_1; y_it != 0; y_it = y_it->next) {
            n_p1->part[n_classe] = append(y_it->val, n_p1->part[n_classe]);
            n_p1->classe[y_it->val] = n_classe;
            traites_1[y_it->val] = 1;
            ntraites++;
          }

          Liste* ylist_2 = liste_equivalents(n, tableau_comptage_2, tableau_comptage_1[x], classe_2, traites_2);
          for(Liste* y_it = ylist_2; y_it != 0; y_it = y_it->next) {
            n_p2->part[n_classe] = append(y_it->val, n_p2->part[n_classe]);
            n_p2->classe[y_it->val] = n_classe;
            traites_2[y_it->val] = 1;
          }

          if (taille(n_p1->part[n_classe]) != taille(n_p2->part[n_classe])) {
            //printf("Incohérence dans le partitionnement.\n");
          //afficher_liste(n_p1->part[n_classe]);
          //  afficher_liste(n_p2->part[n_classe]);
            free(traites_1);
            free(traites_2);
            free_partition(n,p1);
            free_partition(n,p2);
            for(int i=0;i<n;i++) {
              free_liste(tableau_comptage_1[i]);
              free_liste(tableau_comptage_2[i]);
            }
            free(tableau_comptage_1);
            free(tableau_comptage_2);
            PairePartitionsRetour ret = {n_p1,n_p2,-1};
            return ret;
          }

          if (first && (ntraites != taille(classe_1))) {
            changement = 1;
          }
          first = 0;
        }
      }
    }
  }
  free(traites_1);
  free(traites_2);
  free_partition(n,p1);
  free_partition(n,p2);
  for(int i=0;i<n;i++) {
    free_liste(tableau_comptage_1[i]);
    free_liste(tableau_comptage_2[i]);
  }
  free(tableau_comptage_1);
  free(tableau_comptage_2);
  PairePartitionsRetour ret = {n_p1,n_p2,changement};
  return ret;
}

ull partitionnement_stable_bound(int n, int m, int degre_max){
  return n*iteration_partitionnement_bound(n, degre_max, m);
}
PairePartitionsRetour partitionnement_stable(int n, Partition* p1, Partition* p2, Graphe G1, Graphe G2) {
    PairePartitionsRetour resultat = {p1,p2,1};
    while(resultat.retour == 1) {
      resultat = iteration_partitionnement(n, resultat.p1, resultat.p2, G1, G2);
    }
    return resultat;
}

ull choisir_image_bound(int n, int m, int degre_max, int taille_raffinement){
  return 3+taille_raffinement*(4+equivalence_voisinage_bound(n, degre_max, degre_max));
}
int choisir_image(int n, Graphe G1, Graphe G2, int position, char* choisi, int* bijection, Liste* raffinement) {
  int depart = bijection[position];
  Liste* voisins_position = G1[position];
  for(;raffinement != 0; raffinement = raffinement->next) {
    if(raffinement->val > depart && !choisi[raffinement->val]) {
      Liste* voisins_test = G2[raffinement->val];
      bijection[position] = raffinement->val;
      if (equivalence_voisinage(position+1, voisins_position, voisins_test, bijection)) {
        return raffinement->val;
      }
    }
  }
  return -1;
}

long int fact(long int i) {
  long int prod = 1;
  for(int j=i;j>1;j--) {
    prod = prod*i;
  }
  return prod;
}

ull backtracking_bound(int n, Graphe G1, Graphe G2, int raffinement) {

  int degre_max = 0;
  int m = 0;

  for(int i=0;i<n;i++) {
    int t = taille(G1[i]);
    m += t;
    if(t>degre_max) {
      degre_max = t;
    }
  }

  m = m/2;

  ull total = 0;
  total += n*2; // Alloc
  total += 2*partition_degre_bound(n, degre_max);
  if(raffinement == RAFFINEMENT_GLOUTON) {
    total += n*append_bound();
  } else if(raffinement == RAFFINEMENT_PRECIS) {
    total += partitionnement_stable_bound(n, m, degre_max);
  }

  // Long mais nécessaire si on veut obtenir précisément le nombre d'opérations du backtracking (sinon on se contente de fact(n)).
  PairePartitionsRetour x = partitionnement_stable(n, partition_degre(n, G1), partition_degre(n, G2), G1, G2);
  Partition* p1 = x.p1;

  if (x.retour == -1) {
    return total;
  }

  for(int i=0;i<n;i++) {
    total += fact(taille(p1->part[i]));
  }

  return total;
}
int* backtracking(int n, Graphe G1, Graphe G2, int raffinement) {
  int* bijection = malloc(n*sizeof(int));
  char* choisi = malloc(n*sizeof(char));
  for(int i=0;i<n;i++) {
    bijection[i] = -1;
    choisi[i] = 0;
  }
  int position = 0;

  Partition* degres_1 = partition_degre(n, G1);
  Partition* degres_2 = partition_degre(n, G2);

  Partition* p1 = degres_1;
  Partition* p2 = degres_2;

  Liste* restriction = 0;

  if(raffinement == RAFFINEMENT_GLOUTON) {
    for(int i=n-1;i>=0;i--) {
      restriction = append(i,restriction);
    }
  } else if(raffinement == RAFFINEMENT_PRECIS) {
    PairePartitionsRetour x = partitionnement_stable(n, degres_1, degres_2, G1, G2);
    p1 = x.p1;
    p2 = x.p2;
    if (x.retour == -1) {
      return bijection;
    }
  }

  while(position < n) {
    if(raffinement != RAFFINEMENT_GLOUTON) {
      restriction = p2->part[p1->classe[position]];
    }

    int choix = choisir_image(n, G1, G2, position, choisi, bijection, restriction);
    if (choix != -1) { // On avance
      choisi[choix] = 1;
      bijection[position] = choix;
      position++;
    } else { // On recule
      bijection[position] = -1;
      position -= 1;
      choisi[bijection[position]] = 0;

      if(position == -1 || (position == 0 && bijection[position] == n-1)) {
        free_partition(n, p1);
        free_partition(n, p2);
        return bijection;
      }
    }
  }
  free_partition(n, p1);
  free_partition(n, p2);
  return bijection;
}

Graphe scan_graphe(int n) {
  Graphe G = malloc(n*sizeof(Liste*));

  for(int i=0;i<n;i++) {
    int p;
    int val;
    scanf("%i",&p);
    Liste* entree = 0;
    for(int j=0;j<p;j++) {
      scanf("%i",&val);
      entree = append(val, entree);
    }
    G[i] = sort(entree);
  }
  return G;
}

int main() {
  Graphe G1, G2;
  int n, m;
  scanf("%i %i",&n,&m);
  G1 = scan_graphe(n);
  scanf("%i %i",&n,&m);
  G2 = scan_graphe(n);
  int* bij = backtracking(n, G1, G2, RAFFINEMENT_PRECIS);
  //ull total = backtracking_bound(n, G1, G2, RAFFINEMENT_PRECIS);
  //printf("Nombre d'opérations: %llu\n",total);
  if(bij[n-1] == -1) { // Le programme s'est arrêté avant de définir une bijection complète.
    printf("non\n");
  } else {
    printf("oui\n");
    afficher(n,bij);
  }
  free(bij);
  free_graphe(n, G1);
  free_graphe(n, G2);
  return 0;
}



int test() {
  srand(time(NULL));
  int n = 10000;
  float p = 0.0005;
  printf("Génération d'une paire de graphes..\n");
  PaireGraphes x = generer_paire_graphes(n,p, 1);
  //printf("Graphe 1:\n");
//  afficher_g(n,x.g1);
//  printf("Graphe 2:\n");
//  afficher_g(n,x.g2);
  printf("Backtracking..\n");
  int* bijection = backtracking(n,x.g1,x.g2,2);
  afficher(n, bijection);
  free_graphe(n, x.g1);
  free_graphe(n, x.g2);

  return 0;
}
