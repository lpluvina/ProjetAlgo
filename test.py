import main
import util

n = 10000
p = 0.005

G1, G2, perm = util.generer_paire_graphes(n,p,True)

print("permutation utilisée lors de la génération:")
print(str(perm))
print("permutation trouvée:")
print(str(main.backtracking_glouton(n, G1, G2, 2)))
