#include <time.h>
#include <stdlib.h>

typedef unsigned long long ull;
typedef struct Liste {
  int val;
  struct Liste* next;
} Liste;

typedef Liste** Graphe;

typedef struct {
  Liste** part;
  int* classe;
} Partition;

typedef struct {Graphe g1;Graphe g2;} PaireGraphes;
typedef struct {Partition *p1;Partition *p2;int retour;} PairePartitionsRetour;

int mem(int val, Liste* liste);
int taille(Liste* liste);
Liste* append(int val, Liste* lst);
Liste* sort(Liste* lst);

ull mem_bound(int taille_liste);
ull taille_bound(int taille_liste);
ull append_bound();
ull sort_bound(int taille_liste);

void free_partition(int n, Partition* p);
void free_liste(Liste* l);
void free_graphe(int n, Graphe g);

int* permutation_aleatoire(int taille);
Graphe generation_erdos_renyi(int n, float p);
PaireGraphes generer_paire_graphes(int n, float p, int force_isomorphism);

void afficher(int n, int* bijection);
void afficher_g(int n, Graphe g);
void afficher_liste(Liste* l);
